from typing import Any
from typing import Dict
from collections import OrderedDict
from typing import List
import hashlib
import json

def convert_raw_bucket_notification_to_present(
    hub, raw_resource: Dict[str, Any], bucket_name: str
) -> Dict[str, Any]:
    """
    Convert the s3 bucket notification configurations response to a common format

    Args:
        raw_resource: List of s3 bucket notification configurations
        bucket_name: Name of the bucket on which notification needs to be configured.

    Returns:
        A dictionary of s3 bucket notification configurations
    """
    translated_resource = {}
    raw_resource.pop("ResponseMetadata", None)

    if raw_resource:
        translated_resource["name"] = bucket_name
        translated_resource["resource_id"] = bucket_name + "-notifications"
        translated_resource["notifications"] = raw_resource
    return translated_resource

async def convert_s3_bucket_to_present(hub, ctx, bucket_name: str) -> Dict[str, Dict[str, Any]]:
    
    if not bucket_name:
        return {}
    
    resource = hub.tool.boto3.resource.create(ctx, "s3", "Bucket", bucket_name)
    bucket_describe = await hub.tool.boto3.resource.describe(resource)
    lifecycle_config = await hub.exec.boto3.client.s3.get_bucket_lifecycle_configuration(ctx=ctx, Bucket=bucket_name)

    bucket_present = dictionary_mapping(bucket_describe, S3_BUCKET_PARAMETERS)
    
    if lifecycle_config:
        bucket_present["lifecycle_configuration"] = convert_lifecycle_configuration_to_present(hub, config=lifecycle_config["ret"])

    server_side_configuration = await hub.exec.boto3.client.s3.get_bucket_encryption(ctx=ctx, Bucket=bucket_name)
    
    if server_side_configuration:
        bucket_present["server_side_encryption_configuration"] = convert_bucket_encryption_to_present(hub, encryption_config=server_side_configuration["ret"].get("ServerSideEncryptionConfiguration"))

    return bucket_present

def convert_lifecycle_configuration_to_present(hub, config: dict) -> Dict[str, Dict[str, Any]]:
     res = {}

     if not config: 
         return res

     rules = config.get("Rules")
     
     if not rules:
         return res

     res['rules'] = []
     
     for rule in rules:
         res['rules'].append(map_raw_config_rule_to_sls(rule))
     
     return res

def convert_bucket_encryption_to_present(hub, encryption_config: dict) -> Dict[str, Dict[str, Any]]:
     res = {}

     if not encryption_config:
         return res
     
     rules = encryption_config.get("Rules")
     
     if not rules:
         return res

     res['rules'] = []

     for rule in rules:
         rule_instance = dictionary_mapping(rule, S3_BUCKET_ENCRYPTION_RULES_RAW_TO_PRESENT_MAPPING)
         res['rules'].append(rule_instance)

     return res


def convert_bucket_encryption_present_to_raw(hub, encryption_config: dict) -> Dict[str, Dict[str, Any]]:
     res = {}

     if not encryption_config:
         return res
     
     rules = encryption_config.get("rules")
     if not rules:
         return res
     
     res['Rules'] = []

     for rule in rules:
         res['Rules'].append(dictionary_mapping(rule, S3_BUCKET_ENCRYPTION_RULES_PRESENT_TO_RAW_MAPPING))

     return res

def convert_lifecycle_configuration_to_raw(hub, config: dict) -> Dict[str, Dict[str, Any]]:
    
     res = {}

     if not config:
         return res

     rules = config.get("rules")
     if not rules:
         return res

     res['Rules'] = []
     
     for rule in rules:
      res['Rules'].append(map_sls_config_rule_to_raw(rule))

     return res

def map_sls_config_rule_to_raw(rule: dict) -> Dict[str, Dict[str, Any]]:
    res = {}

    if not rule:
        return res
    
    for parameter_old_key, parameter_new_key in LIFECYCLE_CONFIGURATION_RULE_SLS_TO_RAW_MAPPING.items():
       if rule.get(parameter_old_key) is not None:
          if parameter_old_key == "expiration":
             res[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), EXPIRATION_SLS_TO_RAW_MAPPING)
          elif parameter_old_key == "abort_incomplete_multipart_upload":
             res[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), ABORT_INCOMPLETE_MULTIPART_UPLOAD_SLS_TO_RAW_MAPPING)
          elif parameter_old_key == "noncurrent_version_expiration":
             res[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), NONCURRENT_DAYS_RAW_TO_SLS_MAPPING)
          elif parameter_old_key == "filter":
              res[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), FILTER_SLS_TO_RAW_MAPPING)
          else:
              res[parameter_new_key] = rule.get(parameter_old_key)
                      
    return res

def map_raw_config_rule_to_sls(rule) -> Dict[str, Dict[str, Any]]:
    resource_translated = {}
    if not rule:
        return resource_translated

    for parameter_old_key, parameter_new_key in LIFECYCLE_CONFIGURATION_RULE_RAW_TO_SLS_MAPPING.items():
       if rule.get(parameter_old_key) is not None:
          if parameter_old_key == "Expiration":
             resource_translated[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), EXPIRATION_RAW_TO_SLS_MAPPING)
          elif parameter_old_key == "AbortIncompleteMultipartUpload":
             resource_translated[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), ABORT_INCOMPLETE_MULTIPART_UPLOAD_RAW_TO_SLS_MAPPING) 
          elif parameter_old_key == "NoncurrentVersionExpiration":
             resource_translated[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), NONCURRENT_DAYS_SLS_TO_RAW_MAPPING) 
          elif parameter_old_key == "Filter":
             resource_translated[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), FILTER_RAW_TO_SLS_MAPPING)
          else:
             resource_translated[parameter_new_key] = rule.get(parameter_old_key)
    
    return resource_translated


def generate_lifecycle_rules_mutation_list(hub, desired_rules_list: list, actual_rules_list: list) -> Dict[str, Dict[str, Any]]:
    
    if not desired_rules_list or not actual_rules_list: 
        return []

    desired_rules_ids = list(map(lambda rule : rule.get("ID"), desired_rules_list))
    desired_rules_with_ids_dict = dict(zip(desired_rules_ids, desired_rules_list))
    actual_rules_ids = list(map(lambda rule : rule.get("ID"), actual_rules_list))
    actual_rules_with_ids_dict = dict(zip(actual_rules_ids, actual_rules_list))

    desired_rules_ids_set = set(desired_rules_ids)
    actual_rules_ids_set = set(actual_rules_ids)

    intersection_desired_and_actual_ids_set = actual_rules_ids_set.intersection(desired_rules_ids) 
    desired_non_colliding_lifecycle_configuration_ids_set = desired_rules_ids_set - intersection_desired_and_actual_ids_set
    actual_non_colliding_lifecycle_configuration_ids_set = actual_rules_ids_set - intersection_desired_and_actual_ids_set

    colliding_rules_list = [] 

    for id in intersection_desired_and_actual_ids_set:
      # desired state configuration is over-riding the actual state in case of collision
      colliding_rules_list.append(merge(desired_rules_with_ids_dict.get(id), actual_rules_with_ids_dict.get(id)))
    
    desired_non_colliding_rules_list = []
    
    for id in desired_non_colliding_lifecycle_configuration_ids_set:
      desired_non_colliding_rules_list.append(desired_rules_with_ids_dict.get(id))
    
    actual_non_colliding_lifecycle_configuration_rules_list = []
    
    for id in actual_non_colliding_lifecycle_configuration_ids_set:
      actual_non_colliding_lifecycle_configuration_rules_list.append(actual_rules_with_ids_dict.get(id))

    return colliding_rules_list + desired_non_colliding_rules_list + actual_non_colliding_lifecycle_configuration_rules_list 

def merge(dict1, dict2):
    res = {}
    for k in set(dict1.keys()).union(dict2.keys()):
        if k in dict1 and k in dict2:
            if isinstance(dict1[k], dict) and isinstance(dict2[k], dict):
                 res[k] =  mergedicts(dict1[k], dict2[k])
            else:
                # Value from first dict overrides one in first and we move on.
                res[k] = dict1[k]
        elif k in dict1:
            res[k] = dict1[k]
        else:
            res[k] = dict2[k]

    return res

def convert_raw_s3_bucket_to_present(hub, item) -> Dict[str, Dict[str, Any]]:
  res = []

  if not item:
    return res

  for parameter_old_key, parameter_new_key in S3_BUCKET_PARAMETERS.items():
            if item.get(parameter_old_key) is not None:
                res.append({parameter_new_key : item.get(parameter_old_key)})

  return res

def dictionary_mapping(item, mappingDict) -> Dict[str, Dict[str, Any]]:

  res = {}

  if not item or not mappingDict:
    return res

  for parameter_old_key, parameter_new_key in mappingDict.items():
            if item.get(parameter_old_key) is not None:
                 res[parameter_new_key] = item.get(parameter_old_key)

  return res

def hash_dictionary(d):
    d_hashable = immutify_dictionary(d)
    s_hashable = json.dumps(d_hashable).encode("utf-8")
    m = hashlib.sha256(s_hashable).hexdigest()
    return m

def immutify_dictionary(d):
    d_new = {}
    for k, v in d.items():
    
    # immutify any lists
        if isinstance(v, list):
            d_new[k] = tuple(v)

    # recursion nested dicts
        elif isinstance(v, dict):
            d_new[k] = immutify_dictionary(v)

        else:
            d_new[k] = v
    return dict(sorted(d_new.items(), key=lambda item: item[0]))

LIFECYCLE_CONFIGURATION_RULE_RAW_TO_SLS_MAPPING = OrderedDict(
        {
            "Expiration": "expiration",
            "ID": "id",
            "Filter": "filter",
            "Status": "status",
            "AbortIncompleteMultipartUpload" : "abort_incomplete_multipart_upload",
            "NoncurrentVersionExpiration" : "noncurrent_version_expiration"
        }
    )

LIFECYCLE_CONFIGURATION_RULE_SLS_TO_RAW_MAPPING = {value:key for key, value in LIFECYCLE_CONFIGURATION_RULE_RAW_TO_SLS_MAPPING.items()} 

FILTER_RAW_TO_SLS_MAPPING = OrderedDict(
         {
                "Prefix": "prefix"
         })
         
FILTER_SLS_TO_RAW_MAPPING = {value:key for key, value in FILTER_RAW_TO_SLS_MAPPING.items()}

EXPIRATION_RAW_TO_SLS_MAPPING = OrderedDict(
         {
                "Days" : "days"
         }
  )

EXPIRATION_SLS_TO_RAW_MAPPING = {value:key for key, value in EXPIRATION_RAW_TO_SLS_MAPPING.items()}

ABORT_INCOMPLETE_MULTIPART_UPLOAD_RAW_TO_SLS_MAPPING = OrderedDict(
         {
                "DaysAfterInitiation": "days_after_initiation"
         })

ABORT_INCOMPLETE_MULTIPART_UPLOAD_SLS_TO_RAW_MAPPING = {value:key for key, value in ABORT_INCOMPLETE_MULTIPART_UPLOAD_RAW_TO_SLS_MAPPING.items()}
         
NONCURRENT_DAYS_SLS_TO_RAW_MAPPING = OrderedDict(
         {
                "NoncurrentDays" : "noncurrent_days"
         }
  )

NONCURRENT_DAYS_RAW_TO_SLS_MAPPING = {value:key for key, value in NONCURRENT_DAYS_SLS_TO_RAW_MAPPING.items()} 

S3_BUCKET_PARAMETERS = OrderedDict(
            {
                "Name": "name",
                "CreationDate": "creation_date",
            }
        )

S3_BUCKET_ENCRYPTION_RULES_RAW_TO_PRESENT_MAPPING = OrderedDict(
            {
                 "ApplyServerSideEncryptionByDefault" : "apply_server_side_encryption_by_default",
                 "BucketKeyEnabled" : "bucket_key_enabled"
            }
        )

S3_BUCKET_ENCRYPTION_RULES_PRESENT_TO_RAW_MAPPING = {value:key for key, value in S3_BUCKET_ENCRYPTION_RULES_RAW_TO_PRESENT_MAPPING.items()}