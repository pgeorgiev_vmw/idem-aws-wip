{%  for i in range(10) %}
idem_aws_demo_user_{{i}}:
  aws.iam.user.absent
{%  endfor -%}

my_role:
  aws.iam.role.absent

my_idem_vpc:
  aws.ec2.vpc.absent:
    - resource_id: vpc-12345678

my_idem_subnet:
  aws.ec2.subnet.absent:
    - resource_id: subnet-12345678
