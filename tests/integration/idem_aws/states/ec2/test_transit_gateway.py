import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_transit_gateway(hub, ctx):
    # Create transit_gateway
    transit_gateway_temp_name = "idem-test-transit-gateway-" + str(uuid.uuid4())
    tags = [{"Key": "Name", "Value": transit_gateway_temp_name}]
    description = "my new transit gateway"

    # Create transit gateway with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.ec2.transit_gateway.present(
        test_ctx, name=transit_gateway_temp_name, description=description, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.ec2.transit_gateway {transit_gateway_temp_name}"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")
    assert description == resource.get("description")
    assert transit_gateway_temp_name == resource.get("name")

    # Create transit gateway
    ret = await hub.states.aws.ec2.transit_gateway.present(
        ctx, name=transit_gateway_temp_name, description=description, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    await hub.tool.ec2_test_util.wait_for_transit_gateway_state(
        ctx, resource_id, "available"
    )

    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        resource.get("tags"), tags
    ), "tags used in the create should match new state"
    assert description == resource.get("description")
    resource_id = resource.get("resource_id")

    # Testing adding cidr blocks and tags
    options = resource.get("options")
    tags.append(
        {
            "Key": f"idem-test-transit-gateway-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-transit-gateway-value-{str(uuid.uuid4())}",
        }
    )

    # update transit gateway with test flag
    ret = await hub.states.aws.ec2.transit_gateway.present(
        test_ctx,
        name=resource_id,
        resource_id=resource_id,
        description=description,
        tags=tags,
        options=options,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        resource.get("tags"), tags
    ), "tags used in the create should match new state"
    assert description == resource.get("description")
    assert options == resource.get("options")

    # update transit gateway in real
    ret = await hub.states.aws.ec2.transit_gateway.present(
        ctx,
        name=resource_id,
        resource_id=resource_id,
        description=description,
        tags=tags,
        options=options,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    await hub.tool.ec2_test_util.wait_for_transit_gateway_state(
        ctx, resource_id, "available"
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        resource.get("tags"), tags
    ), "tags used in the create should match new state"
    assert description == resource.get("description")
    assert options == resource.get("options")

    # Describe transit_gateway
    describe_ret = await hub.states.aws.ec2.transit_gateway.describe(ctx)
    assert resource_id in describe_ret

    # Verify that output format of describe is correct
    assert "aws.ec2.transit_gateway.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.ec2.transit_gateway.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "resource_id" in described_resource_map
    assert "description" in described_resource_map
    assert "tags" in described_resource_map
    assert "options" in described_resource_map
    assert "state" in described_resource_map

    # Delete transit_gateway with test flag
    ret = await hub.states.aws.ec2.transit_gateway.absent(
        test_ctx, name=transit_gateway_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete aws.ec2.transit_gateway {transit_gateway_temp_name}"
        in ret["comment"]
    )

    # Delete transit_gateway
    ret = await hub.states.aws.ec2.transit_gateway.absent(
        ctx, name=transit_gateway_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    await hub.tool.ec2_test_util.wait_for_transit_gateway_state(
        ctx, resource_id, "deleted"
    )

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.ec2.transit_gateway.absent(
        ctx, name=transit_gateway_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    if hub.tool.utils.is_running_localstack(ctx):
        assert (
            f"aws.ec2.transit_gateway '{transit_gateway_temp_name}' already absent"
            in ret["comment"]
        )
    else:
        assert (
            f"aws.ec2.transit_gateway '{transit_gateway_temp_name}' is in deleted state."
            in ret["comment"]
        )
