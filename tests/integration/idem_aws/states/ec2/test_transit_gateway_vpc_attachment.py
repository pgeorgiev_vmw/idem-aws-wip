import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_transit_gateway_vpc_attachment(
    hub, ctx, aws_ec2_vpc, aws_ec2_subnet, aws_ec2_transit_gateway
):
    # Create transit_gateway vpc attachment
    transit_gateway_vpc_attachment_temp_name = (
        "idem-test-transit-gateway-vpc-attachment" + str(uuid.uuid4())
    )
    tags = [{"Key": "Name", "Value": transit_gateway_vpc_attachment_temp_name}]

    # Create transit_gateway vpc attachment with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    transit_gateway_id = aws_ec2_transit_gateway.get("resource_id")
    vpc_id = aws_ec2_vpc.get("VpcId")
    subnet_ids = [aws_ec2_subnet.get("SubnetId")]
    ret = await hub.states.aws.ec2.transit_gateway_vpc_attachment.present(
        test_ctx,
        name=transit_gateway_vpc_attachment_temp_name,
        transit_gateway=transit_gateway_id,
        vpc_id=vpc_id,
        subnet_ids=subnet_ids,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.ec2.transit_gateway_vpc_attachment {transit_gateway_vpc_attachment_temp_name}"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        resource.get("tags"), tags
    ), "tags used in the create should match new state"
    assert_transit_gateway_vpc_attachment(
        resource, transit_gateway_id, vpc_id, subnet_ids
    )

    # Create transit_gateway vpc attachment in real
    ret = await hub.states.aws.ec2.transit_gateway_vpc_attachment.present(
        ctx,
        name=transit_gateway_vpc_attachment_temp_name,
        transit_gateway=aws_ec2_transit_gateway.get("resource_id"),
        vpc_id=aws_ec2_vpc.get("VpcId"),
        subnet_ids=[aws_ec2_subnet.get("SubnetId")],
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    resource_id = resource.get("resource_id")
    await hub.tool.ec2_test_util.wait_for_transit_gateway_vpc_state(
        ctx, resource_id, "available"
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        resource.get("tags"), tags
    ), "tags used in the create should match new state"
    assert_transit_gateway_vpc_attachment(
        resource, transit_gateway_id, vpc_id, subnet_ids
    )
    # Testing adding tags and updating options
    options = resource.get("options")
    options["DnsSupport"] = "disable"
    tags.append(
        {
            "Key": f"idem-test-transit-gateway-vpc-attachment-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-transit-gateway-vpc-attachment-value-{str(uuid.uuid4())}",
        }
    )

    # Update tags in test context
    ret = await hub.states.aws.ec2.transit_gateway_vpc_attachment.present(
        test_ctx,
        name=transit_gateway_vpc_attachment_temp_name,
        resource_id=resource_id,
        transit_gateway=transit_gateway_id,
        vpc_id=vpc_id,
        subnet_ids=subnet_ids,
        tags=tags,
        options=options,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        resource.get("tags"), tags
    ), "tags used in the create should match new state"
    assert_transit_gateway_vpc_attachment(
        resource, transit_gateway_id, vpc_id, subnet_ids
    )
    assert options == resource.get("options")
    # Update tags in real
    ret = await hub.states.aws.ec2.transit_gateway_vpc_attachment.present(
        ctx,
        name=transit_gateway_vpc_attachment_temp_name,
        resource_id=resource_id,
        transit_gateway=transit_gateway_id,
        vpc_id=vpc_id,
        subnet_ids=subnet_ids,
        tags=tags,
        options=options,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    await hub.tool.ec2_test_util.wait_for_transit_gateway_vpc_state(
        ctx, resource_id, "available"
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        resource.get("tags"), tags
    ), "tags used in the create should match new state"
    assert_transit_gateway_vpc_attachment(
        resource, transit_gateway_id, vpc_id, subnet_ids
    )
    assert options == resource.get("options")

    # Describe transit_gateway_vpc_attachment
    describe_ret = await hub.states.aws.ec2.transit_gateway_vpc_attachment.describe(ctx)
    assert resource_id in describe_ret
    assert describe_ret.get(resource_id) and describe_ret.get(resource_id).get(
        "aws.ec2.transit_gateway_vpc_attachment.present"
    )
    resource = describe_ret.get(resource_id).get(
        "aws.ec2.transit_gateway_vpc_attachment.present"
    )
    described_resource = dict(ChainMap(*resource))
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        described_resource.get("tags"), tags
    ), "tags used in the create should match new state"
    assert_transit_gateway_vpc_attachment(
        described_resource, transit_gateway_id, vpc_id, subnet_ids
    )
    assert "state" in described_resource
    assert options == described_resource.get("options")

    # Delete transit_gateway_vpc_attachment in test context
    ret = await hub.states.aws.ec2.transit_gateway_vpc_attachment.absent(
        test_ctx, name=transit_gateway_vpc_attachment_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        f"Would delete aws.ec2.transit_gateway_vpc_attachment {transit_gateway_vpc_attachment_temp_name}"
        in ret["comment"]
    )

    # Delete transit_gateway_vpc_attachment
    ret = await hub.states.aws.ec2.transit_gateway_vpc_attachment.absent(
        ctx, name=transit_gateway_vpc_attachment_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    await hub.tool.ec2_test_util.wait_for_transit_gateway_vpc_state(
        ctx, resource_id, "deleted"
    )

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.ec2.transit_gateway_vpc_attachment.absent(
        ctx, name=transit_gateway_vpc_attachment_temp_name, resource_id=resource_id
    )
    if hub.tool.utils.is_running_localstack(ctx):
        assert (
            f"aws.ec2.transit_gateway_vpc_attachment '{transit_gateway_vpc_attachment_temp_name}' already absent"
            in ret["comment"]
        )
    else:
        assert (
            f"aws.ec2.transit_gateway_vpc_attachment '{transit_gateway_vpc_attachment_temp_name}' is in deleted state."
            in ret["comment"]
        )
    assert not ret["old_state"] and not ret["new_state"]


def assert_transit_gateway_vpc_attachment(
    resource, transit_gateway, vpc_id, subnet_ids
):
    assert transit_gateway == resource.get("transit_gateway")
    assert vpc_id == resource.get("vpc_id")
    assert subnet_ids == resource.get("subnet_ids")
