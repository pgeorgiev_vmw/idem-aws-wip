import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_internet_gateway(hub, ctx, aws_ec2_vpc):
    # Create internet_gateway
    temp_igw_name = "idem-test-internet-gateway-" + str(uuid.uuid4())
    tags = [
        {"Key": "Name", "Value": temp_igw_name},
    ]
    vpc_id = aws_ec2_vpc.get("VpcId")

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Dry Run.
    ret = await hub.states.aws.ec2.internet_gateway.present(
        test_ctx,
        name=temp_igw_name,
        vpc_id=[vpc_id],
        tags=tags,
    )
    assert tags == ret["new_state"]["tags"]
    assert vpc_id == ret["new_state"]["vpc_id"][0]
    assert (
        f"Would create aws.ec2.internet_gateway '{temp_igw_name}' and attach to vpc '{vpc_id}'"
        in ret["comment"]
    )

    # When vpc_id passed, resource is created and attached to vpc
    ret = await hub.states.aws.ec2.internet_gateway.present(
        ctx,
        name=temp_igw_name,
        vpc_id=[vpc_id],
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret["new_state"]
    created_internet_gateway_id = ret["new_state"]["resource_id"]
    assert tags == ret["new_state"]["tags"]
    assert vpc_id == ret["new_state"]["vpc_id"][0]
    assert f"Created '{temp_igw_name}' and attached to vpc '{vpc_id}'" in ret["comment"]

    # Verify that created internet_gateway is present
    describe_ret = await hub.states.aws.ec2.internet_gateway.describe(ctx)
    assert created_internet_gateway_id in describe_ret
    assert "aws.ec2.internet_gateway.present" in describe_ret.get(
        created_internet_gateway_id
    )
    described_resource = describe_ret.get(created_internet_gateway_id).get(
        "aws.ec2.internet_gateway.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "vpc_id" in described_resource_map
    assert vpc_id == described_resource_map["vpc_id"][0]
    assert tags == described_resource_map.get("tags")

    # Dry Run. Invoke present without vpc_id to verify that there are no changes in attachments.
    ret = await hub.states.aws.ec2.internet_gateway.present(
        test_ctx,
        resource_id=created_internet_gateway_id,
        name=created_internet_gateway_id,
    )
    assert vpc_id == ret["new_state"]["vpc_id"][0]

    # Dry run. Invoke present with empty vpc_id to verify that VPC would be detached from internet gateway.
    ret = await hub.states.aws.ec2.internet_gateway.present(
        test_ctx,
        resource_id=created_internet_gateway_id,
        vpc_id=[],
        name=created_internet_gateway_id,
    )
    assert ret["new_state"]["vpc_id"] == []

    # Invoke present without vpc_id to verify that there are no changes in attachments. Also, update tags
    new_tags = [
        {"Key": "new-name", "Value": created_internet_gateway_id},
    ]
    ret = await hub.states.aws.ec2.internet_gateway.present(
        ctx,
        resource_id=created_internet_gateway_id,
        name=created_internet_gateway_id,
        tags=new_tags,
    )
    assert [vpc_id] == ret["new_state"]["vpc_id"]
    assert ret["result"], ret["comment"]
    assert (
        f"'No change in attachments for {created_internet_gateway_id}'"
        in ret["comment"]
    )
    assert new_tags == ret["new_state"]["tags"]

    # Invoke present with empty vpc_id to verify that VPC would be detached from internet gateway.
    ret = await hub.states.aws.ec2.internet_gateway.present(
        ctx,
        resource_id=created_internet_gateway_id,
        vpc_id=[],
        name=created_internet_gateway_id,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"'{created_internet_gateway_id}' detached from vpc '{vpc_id}'"
        in ret["comment"]
    )
    assert ret["old_state"], ret["new_state"]
    assert ret["old_state"]["attachments"] and "attachments" not in ret["new_state"]

    # Attach to same vpc again
    ret = await hub.states.aws.ec2.internet_gateway.present(
        test_ctx,
        name=created_internet_gateway_id,
        resource_id=created_internet_gateway_id,
        vpc_id=[vpc_id],
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert [vpc_id] == ret["new_state"]["vpc_id"]

    # Attach to same vpc again
    ret = await hub.states.aws.ec2.internet_gateway.present(
        ctx,
        name=created_internet_gateway_id,
        resource_id=created_internet_gateway_id,
        vpc_id=[vpc_id],
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"'{created_internet_gateway_id}' attached to vpc '{vpc_id}'" in ret["comment"]
    )

    # Attach to different vpc, will first detach from attached vpc and then attach to this new vpc
    ret = await hub.states.aws.ec2.internet_gateway.present(
        ctx,
        name=created_internet_gateway_id,
        resource_id=created_internet_gateway_id,
        vpc_id=["invalid"],
        tags=new_tags,
    )
    assert not ret["result"], ret["comment"]
    if hub.tool.utils.is_running_localstack(ctx):
        assert (
            f"ClientError: An error occurred (InvalidVpcID.NotFound) when calling the AttachInternetGateway "
            f"operation: VpcID invalid does not exist." in ret["comment"]
        )
    else:
        assert (
            f"ClientError: An error occurred (InvalidVpcId.Malformed) when calling the AttachInternetGateway "
            f'operation: Invalid id: "invalid" (expecting "vpc-...")' in ret["comment"]
        )

    # Invoke delete with test flag.
    ret = await hub.states.aws.ec2.internet_gateway.absent(
        test_ctx, name=temp_igw_name, resource_id=created_internet_gateway_id
    )
    assert ret["result"], ret["comment"]
    assert ret["changes"]["old"] and not ret["changes"].get("new")
    assert (
        f"Would delete aws.ec2.internet_gateway '{created_internet_gateway_id}'"
        in ret["comment"]
    )

    # Invoke delete
    ret = await hub.states.aws.ec2.internet_gateway.absent(
        ctx, name=created_internet_gateway_id, resource_id=created_internet_gateway_id
    )
    assert ret["result"], ret["comment"]
    assert ret["changes"]["old"] and not ret["changes"].get("new")
    assert f"Deleted '{created_internet_gateway_id}'" in ret["comment"]

    # Delete the same instance again
    ret = await hub.states.aws.ec2.internet_gateway.absent(
        ctx, name=created_internet_gateway_id, resource_id=created_internet_gateway_id
    )
    assert f"'{created_internet_gateway_id}' already absent" in ret["comment"]
