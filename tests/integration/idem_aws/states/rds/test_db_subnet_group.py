import copy
import uuid

import pytest


# IT tests against a AWS endpoint. Created a pipeline in gitlab-ci. For every new change, trigger that pipeline.
@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_db_subnet_group(hub, ctx, aws_ec2_subnet, aws_ec2_subnet_2, aws_ec2_vpc):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    # Create DB Subnet Group
    db_subnet_group_name = "idem-test-db-subnet-group-" + str(uuid.uuid4())
    tags = [
        {"Key": "Name", "Value": db_subnet_group_name},
    ]
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Dry run
    ret = await hub.states.aws.rds.db_subnet_group.present(
        test_ctx,
        name=db_subnet_group_name,
        db_subnet_group_description="For testing idem plugin",
        subnets=[aws_ec2_subnet.get("SubnetId")],
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert (
        f"Would create aws.rds.db_subnet_group '{db_subnet_group_name}'"
        in ret["comment"]
    )

    subnets = [aws_ec2_subnet.get("SubnetId"), aws_ec2_subnet_2.get("SubnetId")]
    # Create DB Subnet Group.
    ret = await hub.states.aws.rds.db_subnet_group.present(
        ctx,
        name=db_subnet_group_name,
        db_subnet_group_description="For testing idem plugin",
        subnets=subnets,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Created '{db_subnet_group_name}'" in ret["comment"]
    resource = ret["new_state"]
    assert resource["name"] == db_subnet_group_name
    assert resource["db_subnet_group_description"] == "For testing idem plugin"
    assert tags == resource["tags"]
    assert resource["subnets"]
    assert len(resource["subnets"]) == 2
    assert set(subnets) == set(resource["subnets"])
    resource_id = resource["resource_id"]

    # Describe instance
    describe_ret = await hub.states.aws.rds.db_subnet_group.describe(ctx)
    assert resource_id in describe_ret
    resource = describe_ret[resource_id].get("aws.rds.db_subnet_group.present")
    assert db_subnet_group_name == resource[0].get("name")
    assert tags == resource[0]["tags"]
    assert resource[0]["db_subnet_group_description"] == "For testing idem plugin"
    assert resource[0]["subnets"]
    assert len(resource[0]["subnets"]) == 2
    assert set(subnets) == set(resource[0]["subnets"])

    # Update no changes to tags with test as true
    ret = await hub.states.aws.rds.db_subnet_group.present(
        test_ctx,
        name=db_subnet_group_name,
        resource_id=resource_id,
        db_subnet_group_description="For testing idem plugin. Dry Run.",
        subnets=subnets,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret["new_state"]
    assert (
        resource["db_subnet_group_description"] == "For testing idem plugin. Dry Run."
    )

    # no changes to tags
    ret = await hub.states.aws.rds.db_subnet_group.present(
        ctx,
        name=db_subnet_group_name,
        resource_id=resource_id,
        db_subnet_group_description="For testing idem plugin",
        subnets=subnets,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"'{db_subnet_group_name}' already exists" in ret["comment"]
    resource = ret["new_state"]
    assert db_subnet_group_name == resource.get("name")
    assert resource["subnets"]
    assert len(resource["subnets"]) == 2
    assert set(subnets) == set(resource["subnets"])
    assert resource["db_subnet_group_description"] == "For testing idem plugin"

    # Updating tags
    new_tags = [
        {"Key": "Name", "Value": "Updated"},
    ]

    # Dry Run with test as true.
    ret = await hub.states.aws.rds.db_subnet_group.present(
        test_ctx,
        name=db_subnet_group_name,
        resource_id=resource_id,
        db_subnet_group_description="For testing idem plugin",
        subnets=subnets,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["new_state"]["tags"] == new_tags

    # Updating tags.
    ret = await hub.states.aws.rds.db_subnet_group.present(
        ctx,
        name=db_subnet_group_name,
        resource_id=resource_id,
        db_subnet_group_description="For testing idem plugin",
        subnets=subnets,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["new_state"]["tags"] == new_tags
    assert f"Updated '{db_subnet_group_name}'" in ret["comment"]
    assert ret["new_state"]["subnets"]
    assert len(ret["new_state"]["subnets"]) == 2
    assert set(subnets) == set(ret["new_state"]["subnets"])

    # Describe instance
    describe_ret = await hub.states.aws.rds.db_subnet_group.describe(ctx)
    assert resource_id in describe_ret
    assert describe_ret.get(resource_id) and describe_ret.get(resource_id).get(
        "aws.rds.db_subnet_group.present"
    )
    resource = describe_ret.get(resource_id).get("aws.rds.db_subnet_group.present")
    describe_tags = None
    for item in resource:
        if "tags" in item:
            describe_tags = item.get("tags")

    assert describe_tags == new_tags

    # removing tags with test as true.
    ret = await hub.states.aws.rds.db_subnet_group.present(
        test_ctx,
        name=db_subnet_group_name,
        resource_id=resource_id,
        db_subnet_group_description="For testing idem plugin",
        subnets=subnets,
        tags=[],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["old_state"]["tags"] == new_tags
    assert ret["new_state"]["tags"] == []

    # removing tags
    ret = await hub.states.aws.rds.db_subnet_group.present(
        ctx,
        name=db_subnet_group_name,
        resource_id=resource_id,
        db_subnet_group_description="For testing idem plugin",
        subnets=subnets,
        tags=[],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["old_state"]["tags"] == new_tags
    assert ret["new_state"]["tags"] == []
    assert f"Updated '{db_subnet_group_name}'" in ret["comment"]

    # Delete instance with test as true
    ret = await hub.states.aws.rds.db_subnet_group.absent(
        test_ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete aws.rds.db_subnet_group '{db_subnet_group_name}'"
        in ret["comment"]
    )

    # Delete instance
    ret = await hub.states.aws.rds.db_subnet_group.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted '{db_subnet_group_name}'" in ret["comment"]

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.rds.db_subnet_group.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert f"'{db_subnet_group_name}' already absent" in ret["comment"]
