import uuid
import pytest

from deepdiff import DeepDiff

@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_bucket_lifecycle_create(hub, ctx):
    # Create bucket
    lifecycle_configuration = {
              "rules" : [
                  {
                    "id" : "test-rule",
                    "status" : "Disabled"
                  }]
            }
    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        lifecycle_configuration=lifecycle_configuration,
        create_bucket_configuration={"LocationConstraint": "us-west-1"},
    )

    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    assert ret["new_state"]
    assert not ret["old_state"]
    resource = ret["changes"]["new"]
    assert resource

    resource_id = resource.get("name")
    assert resource_id 
    assert resource.get("lifecycle_configuration")
    assert equals(resource.get("lifecycle_configuration"), lifecycle_configuration)

    #verify lifecycle configuration created
    describe_ret = await hub.states.aws.s3.bucket.describe(ctx)
    
    bucket = describe_ret.get(bucket_temp_name)
    assert bucket
    assert bucket.get("aws.s3.bucket.present")

    sls_items = bucket.get("aws.s3.bucket.present")
    assert verify_lifecycle_configuration_in_sls_items(lifecycle_configuration, sls_items)


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_bucket_lifecycle_update_with_no_divergence(hub, ctx):
    # Create bucket
    lifecycle_configuration = {
              "rules" : [
                  {
                    "id" : "test-rule",
                    "status" : "Disabled"
                  }]
            }

    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())
    
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        lifecycle_configuration=lifecycle_configuration,
        create_bucket_configuration={"LocationConstraint": "us-west-1"},
    )

    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    assert ret["new_state"]
    assert not ret["old_state"]
    resource = ret["changes"]["new"]
    assert resource

    #update with the same state
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        lifecycle_configuration=lifecycle_configuration,
        create_bucket_configuration={"LocationConstraint": "us-west-1"},
    )
    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and not ret["changes"].get("new")
    assert ret["new_state"]
    assert ret["old_state"]
    assert equals(ret["new_state"], ret["old_state"])

@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_add_lifecycle_config_to_already_existing_bucket(hub, ctx):
    # Create bucket
    lifecycle_configuration = {
              "rules" : [
                  {
                    "id" : "test-rule",
                    "status" : "Disabled"
                  }]
            }
    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())
   
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        create_bucket_configuration={"LocationConstraint": "us-west-1"},
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    resource = ret["changes"]["new"]
    assert resource
    assert not resource.get("lifecycle_configuration")
    prior_update_state = ret["changes"]["new"]
    
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        lifecycle_configuration=lifecycle_configuration
    )

    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    assert not ret["old_state"].get("lifecycle_configuration")
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    assert ret["new_state"].get("lifecycle_configuration")
    assert equals(ret["new_state"].get("lifecycle_configuration"), lifecycle_configuration)


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_bucket_lifecycle_remove(hub, ctx):
    lifecycle_configuration = {
              "rules" : [
                  {
                    "id" : "test-rule",
                    "status" : "Disabled"
                  }]
            }
    
    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())

    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        lifecycle_configuration=lifecycle_configuration,
        create_bucket_configuration={"LocationConstraint": "us-west-1"},
    )

    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    resource = ret["changes"]["new"]
    resource_id = resource.get("name")
    assert resource_id
    assert resource.get("lifecycle_configuration")
    assert ret["new_state"]
    assert not ret["old_state"]

    # Delete instance
    ret = await hub.states.aws.s3.bucket.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret["changes"].get("old") and not ret["changes"].get("new")
    assert not ret["new_state"]
    assert ret["old_state"]

@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_mutate_lifecycle_config_of_existing_bucket(hub, ctx):
    
    # Create bucket
    lifecycle_configuration_initial = {
              "rules" : [
                  {
                    "id" : "test-rule",
                    "status" : "Disabled",
                    "noncurrent_version_expiration" : {
                        "noncurrent_days" : 10
                      }
                  }]
            }

    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())
    
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        lifecycle_configuration=lifecycle_configuration_initial
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    resource = ret["changes"]["new"]
    assert resource
    assert resource.get("lifecycle_configuration")
    assert equals(resource.get("lifecycle_configuration"), lifecycle_configuration_initial)

    #mutate lifecycle config
    lifecycle_configuration_mutated = {
              "rules" : [
                  { 
                    "id" : "test-rule",
                    "expiration" : {
                          "days" : 30
                        },
                    "status" : "Enabled"
                  }]
            }

    expected_mutated_lifecycle_configuration = {
              "rules" : [
                  {
                    "id" : "test-rule",
                    "status" : "Enabled",
                    "expiration" : {
                          "days" : 30
                    },
                    "noncurrent_version_expiration" : {
                        "noncurrent_days" : 10
                      }
                  }]
            }

    ret = await hub.states.aws.s3.bucket.present(
        ctx,    
        name=bucket_temp_name,
        lifecycle_configuration=lifecycle_configuration_mutated
    )

    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    assert ret["old_state"].get("lifecycle_configuration")
    assert equals(ret["old_state"].get("lifecycle_configuration"), lifecycle_configuration_initial)
    assert ret["new_state"].get("lifecycle_configuration")
    assert equals(ret["new_state"].get("lifecycle_configuration"), expected_mutated_lifecycle_configuration)
    assert ret["changes"].get("old") and ret["changes"].get("new")

def ignore_order_func(level):
        return 'a' in level.path()

def equals(a: dict, b: dict) -> bool:
    return len(DeepDiff(a, b, ignore_order=True, ignore_order_func=ignore_order_func)) == 0

def verify_lifecycle_configuration_in_sls_items(lifecycle_config: dict, items: list) -> bool:
    if not items:
        return false

    for item in items:
        if list(item.keys())[0] == "lifecycle_configuration":
           return  equals(list(item.values())[0], lifecycle_config)

    return false