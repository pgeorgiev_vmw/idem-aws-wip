import uuid

import pytest

from deepdiff import DeepDiff

@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_bucket_server_side_encryption_create(hub, ctx):
    # Create bucket
    server_side_encryption_configuration_spec = {
              "rules" : [
                  {
                    "apply_server_side_encryption_by_default" : { 
                          "SSEAlgorithm" : "AES256"
                        },
                    "bucket_key_enabled" : True
                  }]
            }
    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())

    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        server_side_encryption_configuration=server_side_encryption_configuration_spec,
        create_bucket_configuration={"LocationConstraint": "us-west-1"},
    )

    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    assert ret["new_state"]
    assert not ret["old_state"]
    resource = ret["changes"]["new"]
    assert resource

    resource_id = resource.get("name")
    assert resource_id 
    assert resource.get("server_side_encryption_configuration")
    assert equals(resource.get("server_side_encryption_configuration"), server_side_encryption_configuration_spec)

    #verify encryption configuration created
    describe_ret = await hub.states.aws.s3.bucket.describe(ctx)
    
    bucket = describe_ret.get(bucket_temp_name)
    assert bucket
    assert bucket.get("aws.s3.bucket.present")

    sls_items = bucket.get("aws.s3.bucket.present")
    assert verify_server_side_encryption_configuration_in_sls_items(server_side_encryption_configuration_spec, sls_items)

@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_bucket_server_side_encryption_update_with_no_divergence(hub, ctx):
    server_side_encryption_configuration_spec = {
              "rules" : [
                  {
                    "apply_server_side_encryption_by_default" : {
                          "SSEAlgorithm" : "AES256"
                        },
                    "bucket_key_enabled" : True
                  }]
            }


    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())
    
    # Create bucket
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        server_side_encryption_configuration=server_side_encryption_configuration_spec,
        create_bucket_configuration={"LocationConstraint": "us-west-1"},
    )

    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    assert ret["new_state"]
    assert not ret["old_state"]
    resource = ret["changes"]["new"]
    assert resource

    #update with the same state
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        server_side_encryption_configuration=server_side_encryption_configuration_spec,
        create_bucket_configuration={"LocationConstraint": "us-west-1"},
    )
    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and not ret["changes"].get("new")
    assert ret["new_state"]
    assert ret["old_state"]
    assert equals(ret["new_state"], ret["old_state"])


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_add_server_side_encryption_config_to_already_existing_bucket(hub, ctx):

    server_side_encryption_configuration_spec = {
              "rules" : [
                  {
                    "apply_server_side_encryption_by_default" : {
                          "SSEAlgorithm" : "AES256"
                        },
                    "bucket_key_enabled" : True
                  }]
            }

    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())
   
    # Create bucket
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        create_bucket_configuration={"LocationConstraint": "us-west-1"},
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    resource = ret["changes"]["new"]
    assert resource
    assert not resource.get("server_side_encryption_configuration")
    prior_update_state = ret["changes"]["new"]
    
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        server_side_encryption_configuration=server_side_encryption_configuration_spec
    )

    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    assert not ret["old_state"].get("server_side_encryption_configuration")
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    assert ret["new_state"].get("server_side_encryption_configuration")
    assert equals(ret["new_state"].get("server_side_encryption_configuration"), server_side_encryption_configuration_spec)

@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_bucket_with_server_side_encryption_remove(hub, ctx):
        
    server_side_encryption_configuration_spec = {
              "rules" : [
                  {
                    "apply_server_side_encryption_by_default" : {
                          "SSEAlgorithm" : "AES256"
                        },
                    "bucket_key_enabled" : True
                  }]
            }
    
    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())

    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        server_side_encryption_configuration=server_side_encryption_configuration_spec,
        create_bucket_configuration={"LocationConstraint": "us-west-1"},
    )

    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    resource = ret["changes"]["new"]
    resource_id = resource.get("name")
    assert resource_id
    assert resource.get("server_side_encryption_configuration")
    assert ret["new_state"]
    assert not ret["old_state"]

    # Delete instance
    ret = await hub.states.aws.s3.bucket.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret["changes"].get("old") and not ret["changes"].get("new")
    assert not ret["new_state"]
    assert ret["old_state"]

@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_mutate_encryption_configuration_config_of_existing_bucket(hub, ctx):
    
    # Create bucket
    server_side_encryption_configuration_initial_spec = {
              "rules" : [
                  {
                    "apply_server_side_encryption_by_default" : {
                          "SSEAlgorithm" : "AES256"
                        },
                    "bucket_key_enabled" : True
                  }]
    }

    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())
    
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        server_side_encryption_configuration=server_side_encryption_configuration_initial_spec
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    resource = ret["changes"]["new"]
    assert resource
    assert resource.get("server_side_encryption_configuration")
    assert equals(resource.get("server_side_encryption_configuration"), server_side_encryption_configuration_initial_spec)

    #mutate server side encryption configuration
    server_side_encryption_configuration_mutation_spec = { 
              "rules" : [
                  {
                    "apply_server_side_encryption_by_default" : {
                          "SSEAlgorithm" : "AES256"
                        },
                    "bucket_key_enabled" : False
                  }]
    }

    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        server_side_encryption_configuration=server_side_encryption_configuration_mutation_spec
    )

    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    assert ret["old_state"].get("server_side_encryption_configuration")
    assert equals(ret["old_state"].get("server_side_encryption_configuration"), server_side_encryption_configuration_initial_spec)
    assert ret["new_state"].get("server_side_encryption_configuration")
    assert equals(ret["new_state"].get("server_side_encryption_configuration"), server_side_encryption_configuration_mutation_spec)
    assert ret["changes"].get("old") and ret["changes"].get("new")

def ignore_order_func(level):
        return 'a' in level.path()

def equals(a:dict, b:dict) -> bool:
    return len(DeepDiff(a, b, ignore_order=True, ignore_order_func=ignore_order_func)) == 0

def verify_server_side_encryption_configuration_in_sls_items(server_side_encryption_configuration :dict, items :list) -> bool:
    if not items:
        return false

    for item in items:
        if list(item.keys())[0] == "server_side_encryption_configuration":
           return  equals(list(item.values())[0], server_side_encryption_configuration)

    return false

