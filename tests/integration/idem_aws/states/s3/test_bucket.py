import uuid

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
@pytest.mark.skip("Test is skipped until boto3 or localstack is updated")
async def test_bucket(hub, ctx):
    # Create bucket
    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        create_bucket_configuration={"LocationConstraint": "us-west-1"},
    )
    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and ret["changes"]["new"]
    resource = ret["changes"]["new"]

    # resource properties shall follow yaml notation
    resource_id = resource.get("name")

    # Describe instance
    # TODO describe here comes back with a RuntimeError for an unknown reason
    describe_ret = await hub.states.aws.s3.bucket.describe(ctx)
    assert resource_id in describe_ret

    # Delete instance
    ret = await hub.states.aws.s3.bucket.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret["changes"]["old"] and not ret["changes"].get("new")
