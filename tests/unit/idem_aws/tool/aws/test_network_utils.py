def test_generate_cidr_request_payload_for_vpc(hub, mock_hub):
    """
    Test generate_cidr_request_payload_for_vpc() can correctly select the useful parameters to format a cidr association
    request payload that can be used during cidr association/disassociation.
    """
    ipv4_cidr_block_association = {
        "AssociationId": "string",
        "CidrBlock": "string",
        "CidrBlockState": {"State": "associated", "StatusMessage": "string"},
    }
    expected_ipv4_payload = {
        "CidrBlock": "string",
    }
    ipv6_cidr_block_association = {
        "AssociationId": "string",
        "Ipv6CidrBlock": "string",
        "Ipv6CidrBlockState": {"State": "associated", "StatusMessage": "string"},
        "Ipv6CidrBlockNetworkBorderGroup": "string",
        "Ipv6Pool": "string",
    }
    expected_ipv6_payload = {
        "Ipv6CidrBlock": "string",
        "Ipv6CidrBlockNetworkBorderGroup": "string",
        "Ipv6Pool": "string",
    }

    mock_hub.tool.aws.network_utils.generate_cidr_request_payload_for_vpc = (
        hub.tool.aws.network_utils.generate_cidr_request_payload_for_vpc
    )

    ipv4_cidr_payload = (
        mock_hub.tool.aws.network_utils.generate_cidr_request_payload_for_vpc(
            ipv4_cidr_block_association, "ipv4"
        )
    )
    assert expected_ipv4_payload == ipv4_cidr_payload
    ipv6_cidr_payload = (
        mock_hub.tool.aws.network_utils.generate_cidr_request_payload_for_vpc(
            ipv6_cidr_block_association, "ipv6"
        )
    )
    assert expected_ipv6_payload == ipv6_cidr_payload
